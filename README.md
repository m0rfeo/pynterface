# Pynterface

This program is a command-line tool designed on Python to manage and view some relevant information about networks on the computer.

## Requirement

- Install Python Module wifimangement_linux.
```
pip install wifimangement_linux 
```

- Give execution permissions to the program.
```
sudo chmod +x pynterface.py
```

## Example of Usage

```
❯ ./pynterface.py
  _____   ___  _ _____ ___ ___ ___ _   ___ ___ 
 | _ \ \ / / \| |_   _| __| _ \ __/_\ / __| __|
 |  _/\ V /| .` | | | | _||   / _/ _ \ (__| _| 
 |_|   |_| |_|\_| |_| |___|_|_\_/_/ \_\___|___|

(Some options need root permissions)

Available Options 
(CTRL+C to exit):
-----------------
        1. Wifi On/Off.
        2. Gateway.
        3. Available Networks.
        4. Interfaces Status.
        5. Wifi Keys Stored on Computer
        6. Generate QR Code of actual network.
        7. Connect to network.
> 
```

## License

Like all my projects this program is licensed under GPL 3.0 License. You can do anything that you want with my work except change license type, I will be graeful if you mention me.

## Misc

Author: m0rfeo

Open to suggestions :)
