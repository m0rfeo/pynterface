#!/usr/bin/env python

# Pynterface provided by m0rfeo, GPL 3.0 License

import wifimangement_linux as wifi
import sys, os

# Function to check if the user want to continue after use one option
def check_exit():
    ask = (input("\nYou want to exit?(y/n): "))
    if ask == "y":
        print("Have a nice day.")
        sys.exit(0)
    elif ask == "n":
        os.system ("clear") 
        interactive_menu()
    else:
        print("[!] Type y/n")
        sys.exit(1)

# Main Menu Function
def interactive_menu():
    print("""  _____   ___  _ _____ ___ ___ ___ _   ___ ___ 
 | _ \ \ / / \| |_   _| __| _ \ __/_\ / __| __|
 |  _/\ V /| .` | | | | _||   / _/ _ \ (__| _| 
 |_|   |_| |_|\_| |_| |___|_|_\_/_/ \_\___|___|

(Some options need root permissions)

Available Options
(CTRL+C to exit):
-----------------
        1. Wifi On/Off.
        2. Gateway.
        3. Available Networks.
        4. Interfaces Status.
        5. Wifi Keys Stored on Computer
        6. Generate QR Code of actual network.
        7. Connect to network.""")
    choice = (input("> "))

    # Options
    if choice == "1":
        print("--------------------")
        choice2 = (input("on/off: "))
        if choice2 == "on":
            wifi.on()
            print("[DONE] Wifi On")
            check_exit()
        elif choice2 == "off":
            wifi.off()
            print("[DONE] Wifi Off")  
            check_exit()
        else:
            print("[!] Type on/off\n")
            sys.exit(1)
    if choice == "2":
        print("--------------------")
        print("Gateway:",wifi.gateway())
        check_exit()
    elif choice == "3":
        print("--------------------")
        print("Wait...\n")
        print(wifi.list())
        check_exit()
    elif choice == "4":
        print("--------------------")
        print(wifi.interface_status())
        print(wifi.interface_config())
        check_exit()
    elif choice == "5":
        print("--------------------")
        print(wifi.print_psk())
        check_exit()
    elif choice == "6":
        print("--------------------")
        print(wifi.share("qr"))
        check_exit()
    elif choice == "7":
        print("--------------------")
        print("1. Know(SSID) \n2. Unkown Network(SSID and Key)")
        choice_end = (input("> "))
        if choice_end == "1":
            print("--------------------")
            net_name = (input("SSID:"))
            wifi.connect(net_name)
            check_exit()
        elif choice_end == "2":
            print("--------------------")
            net_name = (input("SSID:"))
            net_pass = (input("Key:"))
            wifi.connect(net_name,net_pass)
            check_exit()
        else:
            print("[!] Unknown Option\n")
            sys.exit(1)
    else:
        print("[!] Unkown Option")
        sys.exit(1)

# Print Logo at the firts time and enter into interactive_menu. This structure also control if CTRL+C is pressed to exit
try:
    interactive_menu()
except KeyboardInterrupt:
    print("\nHave a nice day.")
    sys.exit(0)



